
## 一、无核显启用硬件编解码
> 注意：机型必须设置为iMacPro1,1

因为本机没有核心显卡
否则Rx 580 将无法开启硬件编解码

## 二、新系统上启用对老旧硬件的驱动支持

> 注意:每次安装/升级系统之后都需要重新执行以下步骤

### 在 sonoma 上启用对 gt710 的支持

1. 关闭 sip
在恢复模式下执行 
```shell
csrutil disable
```

2. 配置 OpenCore 启动

启动参数中加上`amfi_get_out_of_my_way=1`

配置`csr-active-config`为`<data>6F020000</data>`

> [具体值配置参考](https://www.reddit.com/r/hackintosh/comments/y0kdho/whats_the_corresponding_data_value_to_configure/?rdt=62876)
>
> ![sip_config_requirements](./img/sip_config_requirements.png)

```xml
<key>NVRAM</key>
	<dict>
		<key>Add</key>
		<dict>
			<!--省略-->
			<key>7C436110-AB2A-4BBB-A880-FE41995C9F82</key>
			<dict>
				<key>#INFO (prev-lang:kbd)</key>
				<string>en:252 (ABC), set 656e3a323532</string>
				<key>ForceDisplayRotationInEFI</key>
				<integer>0</integer>
				<key>SystemAudioVolume</key>
				<data>Rg==</data>
				<key>boot-args</key>
				<string>-v debug=0x100 keepsyms=1 alcid=1 amfi_get_out_of_my_way=1	</string>
				<key>csr-active-config</key>
				<data>6F020000</data>
				<key>prev-lang:kbd</key>
				<string>en-US:0</string>
				<key>run-efi-updater</key>
				<string>No</string>
			</dict>
		</dict>
</dict>

```

3. 关闭`SecureBootModel`

```xml
<key>Security</key>
		<dict>
			<key>SecureBootModel</key>
			<string>Disabled</string>
</dict>  
```

4. 在 openCore 启动选择界面选择 `resetNVRAM.efi` 来重置`NVRAM`
5. 重置后需要手动把`OC`添加到系统启动项中(使用easy UEFI)
6. 下载`OpenCore-Legacy-Patcher`

> [下载地址](https://dortania.github.io/OpenCore-Legacy-Patcher/)
>
> 要下载带GUI的版本

7. 执行`post-install root patch`

![index](./img/index.png)

![execute](./img/execute.png)

8. 重启
9. 效果图

![system_info](./img/system_info.png)

![graphices_info](./img/graphices_info.png)